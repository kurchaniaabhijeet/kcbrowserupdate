<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.kcbrowserupdate
 * @author      Abhijeet Kurchania kurchaniaabhijeet@gmail.com
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
jimport('joomla.plugin.plugin');

/**
 * Joomla! System Browser Update warning Plugin.
 *
 * @since  1.5
 */
class PlgSystemKCBrowserUpdate extends JPlugin
{
	function onAfterRoute()
	{
		$app=JFactory::getApplication();
		//Joomla DOcument Factory
		$document = JFactory::getDocument();
		 // No need to run script if html is there
                if($document->getType() != 'html') return true;
		$jinput = $app->input;
		$tmpl=$jinput->get('tmpl');
		//No need to run on tmpl component or raw to support ajax
		if(($tmpl=="component")||($tmpl=="raw"))
		{
			return true;
		}
		if($app->isAdmin())
		{
			return true;
			
		}
		        $sitename=JFactory::getConfig()->get( 'sitename');
			$document->addStyleDeclaration('body .buorg {
				   border-bottom:1px solid '.$this->params->get("bordercolor","#a29330").';
				   background-color:'.$this->params->get("bgcolor","#fdf29b").';
			}
		body .buorg div,body .buorg a,body .buorg a:visited {
		    color:'.$this->params->get("textcolor","#000").';
		}');
			$icon=$this->params->get('icon');
			$texttoadd="";
			if($icon=="dark")
			{
				$texttoadd="_dark";
			}
			JFactory::getLanguage()->load('plg_system_kcbrowserupdate',JPATH_ADMINISTRATOR);
			$script='var $buoop = {vs:{i:'.$this->params->get("ie",10).',f:'.$this->params->get("firefox",5).',o:'.$this->params->get("opera",10).',s:'.$this->params->get("safari",5).',c:'.$this->params->get("ch",15).'},reminder:'.$this->params->get("hour",0).',text: "'.JText::sprintf("PLG_SYS_KCBROWSERUPDATE_MESSAGE",$sitename).'<a target=\"_blank\" href=\"https://www.google.com/chrome\"><img src=\"plugins/system/kcbrowserupdate/assets/ch'.$texttoadd.'.png\" >Download Google Chrome</a><a target=\"_blank\" href=\"http://windows.microsoft.com/en-US/internet-explorer/downloads/ie\"><img src=\"plugins/system/kcbrowserupdate/assets/ie'.$texttoadd.'.png\" >Download Internet Explorer</a><a href=\"//www.apple.com/safari/download/\" target=\"_blank\"><img src=\"plugins/system/kcbrowserupdate/assets/sa'.$texttoadd.'.png\" >Download Safari</a><a href=\"//getfirefox.com/\" target=\"_blank\"><img src=\"plugins/system/kcbrowserupdate/assets/ff'.$texttoadd.'.png\" >Download Firefox</a><a href=\"//www.opera.com/download/\" target=\"_blank\"><img src=\"plugins/system/kcbrowserupdate/assets/op'.$texttoadd.'.png\" >Download Opera</a>"}; 
			function $buo_f(){ 
			 var e = document.createElement("script"); 
			 e.src = "//browser-update.org/update.min.js"; 
			 document.body.appendChild(e);
			};
			try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
			catch(e){window.attachEvent("onload", $buo_f)}';
			$document->addScriptDeclaration($script);
			return true;

	}
}
